#include<iostream>
#include<chrono>

#define MAX 100000
int arr[MAX];
void sort(int arr[], int n);

int main()
{
    for(int i =0; i<MAX; i++)
    {
        arr[i] = rand() % 100000;
    }

    std::cout << "Unsorted array: " << std::endl;
    for (int i = 0; i < MAX; i++)
    {
        std::cout << arr[i] << " ";
    }

    auto start = std::chrono::high_resolution_clock::now();
    int n = sizeof(arr) / sizeof(arr[0]);

    sort(arr, n);

    std::cout << std::endl << "Sorted array: " << std::endl;
    for (int i = 0; i < n; i++)
    {
        std::cout << arr[i] << " ";
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << std::endl << "Time Taken: " << elapsed.count() << " s\n";

    return 0;
}

void sort(int arr[], int n)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n-i-1; j++)
        {
            if (arr[j] > arr[j + 1])
                std::swap(arr[j], arr[j + 1]);
        }
    }
}


