#include<iostream>
#include<queue>

void input();
void bfs(int src);

int n,m;
std::vector<int> adj[1001];
bool visited[1001];

int main()
{
    input();
    bfs(1);
    return 0;
}

void input()
{
    std::cin >> n >> m;
    for (int i = 0; i <m; i++)
    {
        int x,y;
        std::cin >> x >> y;
        adj[x].push_back(y);
        adj[y].push_back(x);
    }
}

void bfs(int src)
{
    std::queue<int> q;
    q.push(src);
    visited[src] = true;
    while(q.empty() == false)
    {
        int v = q.front();
        q.pop();
        std::cout << v << " ";

        for (int i:adj[v])
        {
            if (visited[i] == false)
            {
                q.push(i);
                visited[i] = true;
            }
        }
    }
}
