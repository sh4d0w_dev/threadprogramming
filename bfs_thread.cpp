#include <iostream>
#include <queue>
#include <vector>
#include <pthread.h>

using namespace std;

#define MAX_THREADS 6

const int dx[] = {-1, 1, 0, 0};
const int dy[] = {0, 0, -1, 1};

struct ThreadData {
    vector<vector<int>> grid;
    pair<int, int> start;
    pair<int, int> end;
    int result;  
};

void* bfsShortestPathThread(void* arg) {
    ThreadData* data = static_cast<ThreadData*>(arg);

    int rows = data->grid.size();
    int cols = data->grid[0].size();

    vector<vector<bool>> visited(rows, vector<bool>(cols, false));
    queue<pair<pair<int, int>, int>> q;

    q.push({data->start, 0});
    visited[data->start.first][data->start.second] = true;

    while (!q.empty()) {
        pair<pair<int, int>, int> current = q.front();
        q.pop();

        if (current.first == data->end) {
            data->result = current.second;
            return nullptr;  // bo thread sau khi da tim duoc ket qua
        }

        for (int i = 0; i < 4; ++i) {
            int nx = current.first.first + dx[i];
            int ny = current.first.second + dy[i];

            if (nx >= 0 && nx < rows && ny >= 0 && ny < cols && !visited[nx][ny] && data->grid[nx][ny] == 0) {
                q.push({{nx, ny}, current.second + 1});
                visited[nx][ny] = true;
            }
        }
    }

    // If no path is found
    data->result = -1;
    return nullptr;
}

void printPath(vector<vector<int>>& grid, pair<int, int> start, pair<int, int> end) {
    int rows = grid.size();
    int cols = grid[0].size();

    vector<vector<bool>> visited(rows, vector<bool>(cols, false));
    vector<vector<pair<int, int>>> parent(rows, vector<pair<int, int>>(cols, {-1, -1}));
    queue<pair<int, int>> q;

    q.push(start);
    visited[start.first][start.second] = true;

    while (!q.empty()) {
        pair<int, int> current = q.front();
        q.pop();

        if (current == end) {
            break;
        }

        for (int i = 0; i < 4; ++i) {
            int nx = current.first + dx[i];
            int ny = current.second + dy[i];

            if (nx >= 0 && nx < rows && ny >= 0 && ny < cols && !visited[nx][ny] && grid[nx][ny] == 0) {
                q.push({nx, ny});
                visited[nx][ny] = true;
                parent[nx][ny] = current;
            }
        }
    }

    vector<pair<int, int>> path;
    pair<int, int> current = end;

    while (current != start) {
        path.push_back(current);
        current = parent[current.first][current.second];
    }

    path.push_back(start);

    for (int i = path.size() - 1; i >= 0; --i) {
        cout << "(" << path[i].first << ", " << path[i].second << ") ";
    }

    cout << endl;
}

int main() {
    vector<vector<int>> grid = {
        {1,1,0,0,1,0,1,1,1,0},
        {0,1,1,1,1,0,0,1,0,0},
        {1,1,1,0,0,0,1,0,1,1},
        {0,1,0,0,1,0,0,0,0,1},
        {0,1,1,1,0,1,1,1,0,0},
        {1,0,0,1,0,1,0,1,1,1},
        {0,1,1,0,1,0,0,1,1,0},
        {0,1,0,1,1,0,0,1,1,0},
        {1,0,0,1,1,0,1,1,0,1},
        {0,1,1,1,1,1,1,1,0,0}
    };

    pair<int, int> start = {3,2};
    pair<int, int> end = {2,5};

    ThreadData threadData = {grid, start, end, 0};  // thiet lap du lieu cho thread

    pthread_t threads[MAX_THREADS];
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (int i = 0; i < MAX_THREADS; ++i) {
        pthread_create(&threads[i], &attr, bfsShortestPathThread, &threadData);
    }

    for (int i = 0; i < MAX_THREADS; ++i) {
        pthread_join(threads[i], nullptr);
    }

    pthread_attr_destroy(&attr);

    if (threadData.result != -1) {
        cout << "Duong ngan nhat: " << threadData.result << endl;
        printPath(grid, start, end);
    } else {
        cout << "Khong the tim duong" << endl;
    }

    return 0;
}
