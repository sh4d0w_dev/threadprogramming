#include <iostream>
#include <queue>
#include <vector>

using namespace std;

// define huong di tren duoi trai phai
const int dx[] = {-1, 1, 0, 0};
const int dy[] = {0, 0, -1, 1};

int bfsShortestPath(vector<vector<int>>& grid, pair<int, int> start, pair<int, int> end) {
    int rows = grid.size();
    int cols = grid[0].size();

    vector<vector<bool>> visited(rows, vector<bool>(cols, false));
    queue<pair<pair<int, int>, int>> q;

    q.push({start, 0});
    visited[start.first][start.second] = true;

    while (!q.empty()) {
        pair<pair<int, int>, int> current = q.front();
        q.pop();

        if (current.first == end) {
            return current.second; 
        }

        for (int i = 0; i < 4; ++i) {
            int nx = current.first.first + dx[i];
            int ny = current.first.second + dy[i];

            if (nx >= 0 && nx < rows && ny >= 0 && ny < cols && !visited[nx][ny] && grid[nx][ny] == 0) {
                q.push({{nx, ny}, current.second + 1});
                visited[nx][ny] = true;
            }
        }
    }

    // neu khong tim thay duong di
    return -1;
}

void printPath(vector<vector<int>>& grid, pair<int, int> start, pair<int, int> end) {
    int rows = grid.size();
    int cols = grid[0].size();

    vector<vector<bool>> visited(rows, vector<bool>(cols, false));
    vector<vector<pair<int, int>>> parent(rows, vector<pair<int, int>>(cols, {-1, -1}));
    queue<pair<int, int>> q;

    q.push(start);
    visited[start.first][start.second] = true;

    while (!q.empty()) {
        pair<int, int> current = q.front();
        q.pop();

        if (current == end) {
            break;
        }

        for (int i = 0; i < 4; ++i) {
            int nx = current.first + dx[i];
            int ny = current.second + dy[i];

            if (nx >= 0 && nx < rows && ny >= 0 && ny < cols && !visited[nx][ny] && grid[nx][ny] == 0) {
                q.push({nx, ny});
                visited[nx][ny] = true;
                parent[nx][ny] = current;
            }
        }
    }

    vector<pair<int, int>> path;
    pair<int, int> current = end;

    while (current != start) {
        path.push_back(current);
        current = parent[current.first][current.second];
    }

    path.push_back(start);

    for (int i = path.size() - 1; i >= 0; --i) {
        cout << "(" << path[i].first << ", " << path[i].second << ") ";
    }

    cout << endl;
}

int main() {
    // Example usage
    vector<vector<int>> grid = {
        {1,1,0,0,1,0,1,1,1,0},
        {0,1,1,1,1,0,0,1,0,0},
        {1,1,1,0,0,0,1,0,1,1},
        {0,1,0,0,1,0,0,0,0,1},
        {0,1,1,1,0,1,1,1,0,0},
        {1,0,0,1,0,1,0,1,1,1},
        {0,1,1,0,1,0,0,1,1,0},
        {0,1,0,1,1,0,0,1,1,0},
        {1,0,0,1,1,0,1,1,0,1},
        {0,1,1,1,1,1,1,1,0,0}
    };

    pair<int, int> start = {3,2};
    pair<int, int> end = {2,5};

    int shortestPath = bfsShortestPath(grid, start, end);

    if (shortestPath != -1) {
        cout << "Duong ngan nhat: " << shortestPath << endl;
        printPath(grid, start, end);
    } else {
        cout << "Khong tim thay duong!" << endl;
    }

    return 0;
}
