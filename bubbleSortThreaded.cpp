#include<iostream>
#include<thread>

#define MAX 100000
#define thread_count 6
int step = 0;
int arr[MAX];

void* bubbleSort(void* arg)
{
    int core = step++;
    for (int i = core * MAX / thread_count; i < (core + 1) * MAX / thread_count; i++)
    {
        for (int j = core * MAX / thread_count; j < (core + 1) * MAX / thread_count - 1; j++)
        {
            if (arr[j] > arr[j + 1])
                std::swap(arr[j], arr[j + 1]);
        }
    }
    return NULL;
}

int main()
{
    for(int i =0; i<MAX; i++)
    {
        arr[i] = rand() % 100000;
    }

    std::cout << "Unsorted array: " << std::endl;
    for (int i = 0; i < MAX; i++)
    {
        std::cout << arr[i] << " ";
    }

    auto start = std::chrono::high_resolution_clock::now();
    int n = sizeof(arr) / sizeof(arr[0]);

    pthread_t threads[thread_count];

    for (int i = 0; i < thread_count; i++)
    {
        int *p;
        pthread_create(&threads[i], NULL, bubbleSort, (void*)p);
    }

    for (int i = 0; i < thread_count; i++)
        pthread_join(threads[i], NULL);

    std::cout << std::endl << "Sorted array: " << std::endl;
    for (int i = 0; i < n; i++)
    {
        std::cout << arr[i] << " ";
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<float> duration = end - start;
    std::cout << std::endl << "Time taken: " << duration.count() << " seconds" << std::endl;


}











